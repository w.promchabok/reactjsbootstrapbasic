import {Route, Routes} from 'react-router-dom';
import Home from "./pages/Home";
import About from './pages/About';
import Cart from './pages/Cart';
import Counter from './pages/Counter';

const App = () => {
  return (
    <Routes>
      <Route path="/" exact={true} Component={Home} />
      <Route path="/about" Component={About} />
      <Route path="/cart" Component={Cart} />
      <Route path="/counter" Component={Counter} />

    </Routes>
  )
}

export default App;

