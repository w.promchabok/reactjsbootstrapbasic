import CartItems from "./CartItems"

const product = [
    {
            image: 'https://dcg.wu.ac.th/wp-content/uploads/2020/05/banana1.jpg',
            name: 'Banana',
            price: 30,
            qty: 5
    },
    {
        image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTv3f3i3jvMClGKyrT3JD9Za6kslgeC5tXcqQH6x-C7hpSQyT268d3vOUzqRhjqmdpXeTk&usqp=CAU',
        name: 'Strawberry',
        price: 100,
        qty: 1
    },
    {
        image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT7OIF7byN4XU6KbR0YSkrxUo-20WjUH1H22nSlah8e0luK2pJ-pyCBgG-KS0WSOBojh-c&usqp=CAU',
        name: 'Mango',
        price: 50,
        qty: 3
    }
    ,
    {
        image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRoXSh7SKnWaBVbDcsghTljSfbNX6uz1DDJkw&usqp=CAU',
        name: 'Apple',
        price: 60,
        qty: 8
    }
]

const Cart = () => {
  return (
    <div className="container">
        <div className="row">
            {            
                product.map((product, index) => {
                    return (                
                        <div className="col-md-4 my-3" key={index}>
                        {/* <CartItems 
                            imgUrl = {product.image}
                            name= {product.name} 
                            price= {product.price}
                            qty= {product.qty} /> */}
                        <CartItems 
                        product = {product} />
                        </div>
                    )
                })
            }
        </div>
    </div>
  )
}

export default Cart
