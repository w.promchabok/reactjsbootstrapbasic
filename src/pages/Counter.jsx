import { useEffect, useState } from "react"

const Counter = () => {

const [counter, setCounter] = useState(0)

useEffect(() => {
    setCounter(50)
    console.log('useEffect')
    return () => {
        console.log('unmount')
    }
},[])
//   let counter = 0;
//   const onchangeCounter = () => {
//     counter = counter + 1
//     console.log(counter)
      
//   }
  
  return (
    <div>
      <h1>Counter: {counter} </h1>
      <button onClick={ () => setCounter( counter + 1 ) } >Counter up</button>
      <button onClick={ () => setCounter( counter - 1 ) } >Counter down</button>

    </div>
  )
}

export default Counter
