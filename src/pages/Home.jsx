import MainLayout from "../components/layouts/MainLayout"

const Home = () => {
  return (
    <MainLayout>
        <h1>Home Page</h1>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloremque est obcaecati temporibus voluptatem eveniet impedit quasi. Deleniti accusantium odit doloribus aliquam adipisci perspiciatis, quod, esse minima consequatur quidem in voluptatem!</p>
    </MainLayout>
  )
}

export default Home
