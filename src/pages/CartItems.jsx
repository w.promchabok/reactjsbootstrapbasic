const CartItems = (props) => {
    console.log(props.product);

    // Object destructuring
    const {image, name, price, qty} = props.product
  return (
    <div className="card">
      <img src={image} className="card-img-top" alt="..." />
      <div className="card-body">
        <h5 className="card-title">{name}</h5>
        <p className="card-text">{price}THB</p>
        <p className="card-text">{qty} items</p>
      </div>
    </div>
  );
};

export default CartItems;
